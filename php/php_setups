#!/bin/bash

# Written by vijaydeep Sharma <vijaydeep.sharma@daffodilsw.com>

#This scripts focus on setting up php and php-fpm evironments on
#ubuntu servers. This script setups the following packages
# * MySQL
# * nginx
# * php-cli
# * php-fpm
# * php-curl
# * php-mbstring
# * php-xml
# * php-gd
# * php-mysql
# * composer
# * unzip
# * certbot

# set up data partition
mount_data_partition() {
    lsblk
    read -p "Enter the data partition device (eg., /dev/sdb): " data_partition
    sudo mkdir -p /mnt/ebsdir
    sudo mkfs.ext4 $data_partition
    echo "UUID=$(sudo blkid -s UUID -o value $data_partition) /mnt/ebsdir ext4 defaults,nofail  0 0" | sudo tee -a /etc/fstab
    sudo mount -a
    echo "Success!! mounted data partition $data_partition on /mnt/ebsdir"
}

# set up swap
mount_swap() {
    lsblk
    read -p "Enter the swap partition device (eg., /dev/sdb): " swap_partition
    sudo mkswap $swap_partition
    echo "UUID=$(sudo blkid -s UUID -o value $swap_partition) none swap sw 0 0" | sudo tee -a /etc/fstab
    sudo mount -a
    echo "Success!! swap created and mounted. It will be available on next reboot."
}

# setup MySql
install_mysql() {
    sudo apt install -y mysql-server
    #sudo mysql_secure_installation
    read -p "Enter a password to setup mysql root user: " mysql_root_password
    read -p "Enter name of the database to be created for project: " mysql_project_db
    read -p "Enter username for the mysql user to be created for for project: " mysql_project_user
    read -p "Enter password for the mysql user to be used for the project: " mysql_project_user_password
    sudo mysql -e "CREATE DATABASE $mysql_project_db DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
    sudo mysql -e "GRANT ALL ON $mysql_project_db.* TO '$mysql_project_user'@'localhost' IDENTIFIED BY '$mysql_project_user_password';"
    sudo mysql -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '$mysql_root_password'; FLUSH PRIVILEGES;"
    echo "Success!! MySQL installed"
}

setup_php_nginx() {
    read -p "Enter the version of php to set up (eg., 7.0, 7.1, 7.3): " php_version
    php="php$php_version"
    sudo add-apt-repository ppa:ondrej/php
    sudo apt install -y nginx $php-cli $php-json $php-opcache $php-mbstring $php-gd $php-mysql $php-xml $php-curl $php-fpm composer unzip
    read -p "Enter the server name string (eg., example.com www.example.com): " server_name_string
    echo """
server {
	listen 80;
	listen [::]:80;

	# change project root according to your requirements
	root /var/www/html;
	# root /mnt/ebsdir/projectname/public

	index index.php index.html index.htm index.nginx-debian.html;

	server_name $server_name_string;

	location / {
		# First attempt to serve request as file, then
		# as directory, then fall back to displaying a 404.
		try_files \$uri \$uri/ /index.php?\$query_string;
	}

	# pass PHP scripts to FastCGI server
	location ~ \.php$ {
		include snippets/fastcgi-php.conf;
	
		# With php-fpm (or other unix sockets):
		fastcgi_pass unix:/var/run/php/$php-fpm.sock;
	}

}
    """ | sudo tee /etc/nginx/sites-available/default
    
    echo "<?php phpinfo() ?>" | sudo tee /var/www/html/index.php
    sudo rm /var/www/html/index.nginx-debian.html
    sudo systemctl reload nginx.service
    echo "Success!! nginx, php installed"
}

setup_new_user() {
    read -p "Enter username for new user (eg., developer): " new_username
    sudo adduser --disabled-password $new_username
    echo "$new_username ALL=(ALL) NOPASSWD:ALL" | sudo tee -a /etc/sudoers
    ssh-keygen
    sudo mkdir /home/$new_username/.ssh
    cat .ssh/id_rsa.pub | sudo tee /home/$new_username/.ssh/authorized_keys
    sudo chmod 600 /home/$new_username/.ssh/authorized_keys
    sudo chown -R $new_username:$new_username /home/$new_username/.ssh
    echo "Please copy the private key bellow and paste in .pem file"
    cat .ssh/id_rsa
    
    read -p "Copied the private key? [Y/N]: " if_copied_key

    if [ "$if_copied_key" == "Y" ]
    then
        echo "cleaning up rsa files..."
        rm .ssh/id_rsa*
    fi

    echo "Success!! User created"
}

setup_ssl_cert() {
    sudo add-apt-repository ppa:certbot/certbot
    sudo apt install -y python-certbot-nginx
    read -p "Enter domains to protect seperated with '-d' (eg., -d example.com -d www.example.com): " domains_string
    sudo certbot --nginx $domains_string 
    sudo certbot renew --dry-run
    echo "Success!! SSL installed"
}

# update system
sudo apt update; sudo apt -y upgrade

read -p "Set up data partition? [Y/N]: " if_data_partition

if [ "$if_data_partition" == "Y" ]
then
    mount_data_partition
fi

read -p "Setup swap? [Y/N]: " if_swap

if [ "$if_swap" == "Y" ]
then
    mount_swap
fi

read -p "Install MySQL database (By default it'll install latest version (currently v5.7) from ubuntu repo)? [Y/N]: " if_install_mysql

if [ "$if_install_mysql" == "Y" ]
then
    install_mysql
fi

setup_php_nginx

read -p "Setup different user for developer? [Y/N]: " if_new_user

if [ "$if_new_user" == "Y" ]
then
    setup_new_user
fi

read -p "Install Let's encrypt SSL Certificate? (Make sure that domian is already pointed to here.) [Y/N]: " if_ssl_cert

if [ "$if_ssl_cert" == "Y" ]
then
    setup_ssl_cert
fi

read -p "Reboot to enable swap? Before rebooting please copy the private ssh key if you created one!! [Y/N]: " if_reboot

if [ "$if_reboot" == "Y" ]
then
    echo "rebooting in 5 Seconds..."
    sleep 5
    sudo reboot
fi
